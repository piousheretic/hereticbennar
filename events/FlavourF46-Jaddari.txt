
namespace = jaddari_missions

#Reform the Cult
country_event = {
	id = jaddari_missions.1
	title = jaddari_missions.1.t
	desc = jaddari_missions.1.d
	picture = POPE_PREACHING_eventPicture
	
	is_triggered_only = yes
	
	#Convert
	option = {
		name = jaddari_missions.1.a
		ai_chance = {
			factor = 40 
			modifier = {
				factor = 2
				NOT = {
					primary_culture = dawn_elf
					primary_culture = sun_elf
					primary_culture = zanite
					primary_culture = brasanni
					primary_culture = bahari
					primary_culture = gelkar
				}
			}
		}

		change_religion = the_jadd
		
		add_country_modifier = {
			name = "conversion_zeal"
			duration = 3650
		}
	}
	
	#Don't convert: Tolerant
	option = {
		name = jaddari_missions.1.b
		ai_chance = {
			factor = 30 
			modifier = {
				factor = 2
				OR = {
					primary_culture = sun_elf
					primary_culture = dawn_elf
				}
			}
		}
	
		add_country_modifier = {
			name = tolerant_society
			duration = 3650
		}
	}
	
	#Don't convert: Harsh
	option = {
		name = jaddari_missions.1.c
		ai_chance = {
			factor = 30 
			modifier = {
				factor = 2
				OR = {
					primary_culture = sun_elf
					primary_culture = dawn_elf
				}
			}
		}
		
		add_country_modifier = {
			name = intolerant_society
			duration = 3650
		}
	}
}

#A harpy march
country_event = {
	id = jaddari_missions.2
	title = jaddari_missions.2.t
	desc = jaddari_missions.2.d
	picture = WESTERNISATION_eventPicture
	
	is_triggered_only = yes
	
	#Accept
	option = {
		name = jaddari_missions.2.a
		ai_chance = { factor = 100 }
		
		2903 = {
			cede_province = F49
			add_core = F49
			remove_core = F46
		}
		2904 = {
			cede_province = F49
			add_core = F49
			remove_core = F46
		}
		2906 = {
			cede_province = F49
			add_core = F49
			remove_core = F46
		}
		2908 = {
			cede_province = F49
			add_core = F49
			remove_core = F46
		}
		2910 = {
			cede_province = F49
			add_core = F49
			remove_core = F46
		}
		2919 = {
			cede_province = F49
			add_core = F49
			remove_core = F46
		}
		2920 = {
			cede_province = F49
			add_core = F49
			remove_core = F46
		}
		2921 = {
			cede_province = F49
			add_core = F49
			remove_core = F46
		}
		4376 = {
			cede_province = F49
			add_core = F49
			remove_core = F46
		}
		F46 = { create_march = F49 }
		F49 = {
			add_country_modifier = {
				name = jaddari_harpy_march
				duration = -1
			}
		}
		hidden_effect = {
			every_country = {
				remove_opinion = {
					who = ROOT
					modifier = root_monstrous
				}
				reverse_remove_opinion = {
					who = ROOT
					modifier = root_monstrous
				}
			}
			remove_opinion = {
				who = F46
				modifier = jaddari_jasiene
			}
			reverse_remove_opinion = {
				who = F46
				modifier = jaddari_jasiene
			}
			F46 = {
				country_event = {
					id = jaddari_missions.3
					days = 1
				}
			}
		}
		
	}
	
	#Reject
	option = {
		name = jaddari_missions.2.b
		ai_chance = { factor = 0 }
	
		add_trust = {
			who = F46
			value = -25
			mutual = yes
		}
	}
}

#Elayenna accepted
country_event = {
	id = jaddari_missions.3
	title = jaddari_missions.3.t
	desc = jaddari_missions.3.d
	picture = WESTERNISATION_eventPicture
	
	is_triggered_only = yes
	
	option = {
		name = jaddari_missions.1.a
	}
}

#Elayenna refused
country_event = {
	id = jaddari_missions.4
	title = jaddari_missions.4.t
	desc = jaddari_missions.4.d
	picture = WESTERNISATION_eventPicture
	
	is_triggered_only = yes
	
	option = {
		name = jaddari_missions.4.a
		
		tooltip = {
			add_trust = {
				who = F49
				value = -25
				mutual = yes
			}
		}
	}
}

#Prep for the expedition
country_event = {
	id = jaddari_missions.5
	title = jaddari_missions.5.t
	desc = jaddari_missions.5.d
	picture = EXPLORERS_eventPicture
	
	is_triggered_only = yes
	
	immediate = { hidden_effect = { country_event = { id = jaddari_missions.6 days = 365 random = 182 } } }
	
	#This expedition will be led by our greatest warriors!
	option = {
		name = jaddari_missions.5.a
		add_manpower = -5
		add_army_tradition = -20
		add_years_of_income = -1
		
		set_country_flag = jaddari_serpentspine_expedition_great_prep
	}
	#Prepare a large and well funded expedition!
	option = {
		name = jaddari_missions.5.b
		add_manpower = -5
		add_years_of_income = -1
		
		set_country_flag = jaddari_serpentspine_expedition_good_prep
	}
	#Send a small group of adventurers
	option = {
		name = jaddari_missions.5.c
		add_manpower = -0.004
		add_years_of_income = -0.5
		
		set_country_flag = jaddari_serpentspine_expedition_no_prep
	}
}

#The goal of the expedition
country_event = {
	id = jaddari_missions.6
	title = jaddari_missions.6.t
	desc = jaddari_missions.6.d
	picture = EXPLORERS_eventPicture
	
	is_triggered_only = yes
	
	#The true enemy is hidden within the ancient holds.
	option = {
		name = jaddari_missions.6.a
		
		hidden_effect = {
			if = {
				limit = { has_country_flag = jaddari_serpentspine_expedition_great_prep }
				random_list = {
					10 = { country_event = { id = jaddari_missions.11 days = 730 random = 3650 } }
					60 = { country_event = { id = jaddari_missions.10 days = 730 random = 3650 } }
					30 = { country_event = { id = jaddari_missions.7 days = 730 random = 3650 } }
				}
			}
			if = {
				limit = { has_country_flag = jaddari_serpentspine_expedition_good_prep }
				random_list = {
					5 = { country_event = { id = jaddari_missions.11 days = 730 random = 3650 } }
					35 = { country_event = { id = jaddari_missions.10 days = 730 random = 3650 } }
					60 = { country_event = { id = jaddari_missions.7 days = 730 random = 3650 } }
				}
			}
			if = {
				limit = { has_country_flag = jaddari_serpentspine_expedition_no_prep }
				random_list = {
					20 = { country_event = { id = jaddari_missions.10 days = 730 random = 1825 } }
					80 = { country_event = { id = jaddari_missions.7 days = 730 random = 3650 } }
				}
			}
		}
	}
	#We must banish the darkness from the caves
	option = {
		name = jaddari_missions.6.b
		
		hidden_effect = {
			if = {
				limit = { has_country_flag = jaddari_serpentspine_expedition_great_prep }
				random_list = {
					90 = { country_event = { id = jaddari_missions.8 days = 730 random = 3650 } }
					10 = { country_event = { id = jaddari_missions.7 days = 730 random = 3650 } }
				}
			}
			if = {
				limit = { has_country_flag = jaddari_serpentspine_expedition_good_prep }
				random_list = {
					70 = { country_event = { id = jaddari_missions.8 days = 730 random = 3650 } }
					30 = { country_event = { id = jaddari_missions.7 days = 730 random = 3650 } }
				}
			}
			if = {
				limit = { has_country_flag = jaddari_serpentspine_expedition_no_prep }
				random_list = {
					40 = { country_event = { id = jaddari_missions.8 days = 730 random = 1825 } }
					60 = { country_event = { id = jaddari_missions.7 days = 730 random = 3650 } }
				}
			}
		}
	}
	#Explore the Dwarovrod
	option = {
		name = jaddari_missions.6.c
		
		hidden_effect = {
			if = {
				limit = { has_country_flag = jaddari_serpentspine_expedition_great_prep }
				country_event = { id = jaddari_missions.9 days = 730 random = 3650 }
			}
			if = {
				limit = { has_country_flag = jaddari_serpentspine_expedition_good_prep }
				random_list = {
					80 = { country_event = { id = jaddari_missions.9 days = 730 random = 3650 } }
					20 = { country_event = { id = jaddari_missions.7 days = 730 random = 3650 } }
				}
			}
			if = {
				limit = { has_country_flag = jaddari_serpentspine_expedition_no_prep }
				random_list = {
					60 = { country_event = { id = jaddari_missions.9 days = 730 random = 1825 } }
					40 = { country_event = { id = jaddari_missions.7 days = 730 random = 3650 } }
				}
			}
		}
	}
}

#We haven't heard anything back
country_event = {
	id = jaddari_missions.7
	title = jaddari_missions.7.t
	desc = jaddari_missions.7.d
	picture = EXPLORERS_eventPicture
	
	is_triggered_only = yes
	
	#That's very sad
	option = {
		name = jaddari_missions.7.a
		
		add_prestige = -10
	}
}

#A return from the caves with many riches!
country_event = {
	id = jaddari_missions.8
	title = jaddari_missions.8.t
	desc = jaddari_missions.8.d
	picture = EXPLORERS_eventPicture
	
	is_triggered_only = yes
	
	#Splendid!
	option = {
		name = jaddari_missions.8.a
		
		add_years_of_income = 2
		if = { limit = { has_country_flag = jaddari_serpentspine_expedition_great_prep } add_army_tradition = 25 add_manpower = 3 }
		if = { limit = { has_country_flag = jaddari_serpentspine_expedition_good_prep } add_army_tradition = 15 add_manpower = 3 }
		if = { limit = { has_country_flag = jaddari_serpentspine_expedition_good_prep } add_army_tradition = 10 add_manpower = 0.003 create_general = { tradition = 80 } }
		add_prestige = 10
	}
}

#Succesful expedition through the tunnels!
country_event = {
	id = jaddari_missions.9
	title = jaddari_missions.9.t
	desc = jaddari_missions.9.d
	picture = EXPLORERS_eventPicture
	
	is_triggered_only = yes
	
	#Splendid!
	option = {
		name = jaddari_missions.9.a
		
		add_years_of_income = 1
		if = { limit = { has_country_flag = jaddari_serpentspine_expedition_great_prep } add_army_tradition = 20 add_manpower = 4 }
		if = { limit = { has_country_flag = jaddari_serpentspine_expedition_good_prep } add_army_tradition = 10 add_manpower = 3 }
		if = { limit = { has_country_flag = jaddari_serpentspine_expedition_good_prep } add_army_tradition = 5 add_manpower = 0.003 }
		add_prestige = 5
	}
}

#Successful return from the hold!
country_event = {
	id = jaddari_missions.10
	title = jaddari_missions.10.t
	desc = jaddari_missions.10.d
	picture = EXPLORERS_eventPicture
	
	is_triggered_only = yes
	
	#Splendid!
	option = {
		name = jaddari_missions.10.a
		
		add_years_of_income = 5
		if = { limit = { has_country_flag = jaddari_serpentspine_expedition_great_prep } add_army_tradition = 25 add_manpower = 3 }
		if = { limit = { has_country_flag = jaddari_serpentspine_expedition_good_prep } add_army_tradition = 15 add_manpower = 3 }
		if = { limit = { has_country_flag = jaddari_serpentspine_expedition_good_prep } add_army_tradition = 10 add_manpower = 0.003 create_general = { tradition = 100 } }
		add_prestige = 10
	}
}

#Amazing success in the hold!
country_event = {
	id = jaddari_missions.11
	title = jaddari_missions.11.t
	desc = jaddari_missions.11.d
	picture = EXPLORERS_eventPicture
	
	is_triggered_only = yes
	
	#Splendid!
	option = {
		name = jaddari_missions.11.a
		
		add_years_of_income = 10
		add_manpower = 3
		if = { limit = { has_country_flag = jaddari_serpentspine_expedition_great_prep } add_army_tradition = 30 }
		else = { add_army_tradition = 20 }
		add_prestige = 20
	}
}



#Fucking AI work
country_event = {
	id = jaddari_missions.1000
	title = jaddari_missions.1.t
	desc = jaddari_missions.1.d
	picture = POPE_PREACHING_eventPicture
	
	is_triggered_only = yes
	fire_only_once = yes
	
	trigger = {
		tag = F46
		ai = yes
	}
	
	#Convert
	option = {
		name = jaddari_missions.1.a
		ai_chance = {
			factor = 1
		}
		
		add_country_modifier = {
			name = jaddari_ai_boost
			duration = 36500
		}
	}
}

